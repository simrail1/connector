const axios = require("axios");

const requester = axios.create({
  baseURL: "https://panel.simrail.eu:8084",
});

module.exports = requester;
