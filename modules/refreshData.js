const serversOpen = require("../apis/serversOpen");
const stationsOpen = require("../apis/stationsOpen");
const trainsOpen = require("../apis/trainsOpen");
const playersOpen = require("../apis/playersOpen");
const logger = require("./logger");
const redis = require("./redis");
const getTrainTimetable = require("../apis/getTrainTimetable");

let stopRefresh = false;

module.exports = async function refreshData() {
  try {
    const servers = await serversOpen();
    stopRefresh = true;
    const steamIds = [];

    for (const server of servers) {
      try {
        logger.debug("Refreshing server: ", server.code);
        await redis.HSET("servers", server.code, JSON.stringify(server));
        logger.debug("Refreshing stations for server: ", server.code);
        const stations = await stationsOpen(server.code);
        await redis.HSET("stations", server.code, JSON.stringify(stations));
        logger.debug("Refreshing trains for server: ", server.code);
        const newTrains = await trainsOpen(server.code);
        const oldTrains = await redis.HGET("trains", server.code);
        let trains = await getTrainDirections(oldTrains, newTrains);
        trains = trains.filter((train) => train !== undefined);
        for (let train of trains) {
          if (!stopRefresh) {
            await getTrainTimetable(train);
          }
          steamIds.push(train.driver);
        }
        stations.forEach((station) => {
          steamIds.push(station.dispatcher);
        });

        logger.debug("Refreshing players for server: ", server.code);
        await playersOpen(steamIds);

        await redis.HSET("trains", server.code, JSON.stringify(trains));
      } catch (error) {
        logger.error("Error refreshing data for server: ", server.code, error);
        console.log(error);
      }
    }

    logger.debug("Checking for non-active servers");
    redis.HGETALL("servers").then((data) => {
      Object.keys(data).forEach(async (key) => {
        const server = JSON.parse(data[key]);
        if (!server.active || server.lastUpdate < Date.now() - 1000 * 60 * 60) {
          logger.debug("Removing non-active server: ", key);
          await redis.HDEL("servers", key);
          await redis.HDEL("stations", key);
          await redis.HDEL("trains", key);
        }
      });
    });
  } catch (error) {
    console.log(error);
    logger.error("Error refreshing data: ", error);
    stopRefresh = false;
  } finally {
    stopRefresh = false;
  }
};

async function getTrainDirections(oldTrainData, newTrainData) {
  oldTrainData = JSON.parse(oldTrainData);
  if (!oldTrainData) {
    return newTrainData;
  }
  return newTrainData.map((train) => {
    const oldTrain = oldTrainData.find((t) => t.id === train.id);
    if (oldTrain) {
      const oldPosition = oldTrain.position;
      const newPosition = train.position;
      const direction =
        oldPosition[0] === newPosition[0]
          ? oldPosition[1] < newPosition[1]
            ? 0
            : 180
          : Math.atan2(
              newPosition[1] - oldPosition[1],
              newPosition[0] - oldPosition[0]
            ) *
            (180 / Math.PI);
      return {
        ...train,
        direction,
      };
    }
    return {
      ...train,
      direction: 0,
    };
  });
}
