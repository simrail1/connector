const axios = require("../modules/axios.js");

module.exports = async (serverCode) => {
  try {
    const { data } = await axios.get("/trains-open?serverCode=" + serverCode);
    if (!data.result) {
      throw data;
    }
    const normalizedData = data.data.map((train) => ({
      id: train.id,
      type: train.Type,
      serverCode: train.ServerCode,
      number: train.TrainNoLocal,
      name: train.TrainName,
      stations: {
        start: train.StartStation,
        end: train.EndStation,
      },
      driver: train.TrainData.ControlledBySteamID,
      position: [train.TrainData.Latititute, train.TrainData.Longitute],
      velocity: Math.round(train.TrainData.Velocity),
      nextSignal: {
        speed: train.TrainData.SignalInFrontSpeed,
        distance: Math.round(train.TrainData.DistanceToSignalInFront),
        name: train.TrainData.SignalInFront
          ? train.TrainData.SignalInFront.split("@")[0]
          : "",
      },
      delay: Math.round(train.TrainData.VDDelayedTimetableIndex / 2),
      inStation: train.TrainData.InBorderStationArea,
      vehicles: train.Vehicles.map((vehicle) => ({
        type: vehicle.split("/")[0],
        model: vehicle.split("/")[1].split("_")[0].split("-")[0],
        fullString: vehicle,
      })),
    }));
    return normalizedData;
  } catch (error) {
    throw error;
  }
};
