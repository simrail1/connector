const axios = require("../modules/axios.js");

module.exports = async (serverCode) => {
  try {
    const { data } = await axios.get("/stations-open?serverCode=" + serverCode);
    if (!data.result) {
      throw data;
    }
    const normalizedData = data.data.map((station) => ({
      id: station.id,
      position: [station.Latititude, station.Longitude],
      name: station.Name,
      prefix: station.Prefix,
      images: [
        station.MainImageURL,
        station.AdditionalImage1URL,
        station.AdditionalImage2URL,
      ],
      difficulty: station.DifficultyLevel,
      dispatcher:
        station.DispatchedBy.length > 0
          ? station.DispatchedBy[0].SteamId
          : null,
    }));
    return normalizedData;
  } catch (error) {
    throw error;
  }
};
