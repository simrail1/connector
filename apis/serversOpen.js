const axios = require("../modules/axios.js");

module.exports = async () => {
  try {
    const { data } = await axios.get("/servers-open");
    if (!data.result) {
      throw data;
    }
    const normalizedData = data.data.map((server) => ({
      code: server.ServerCode,
      name: server.ServerName.split(" ")[0],
      language: server.ServerName.split(" ")[1]
        ? server.ServerName.split(" ")[1].replace("(", "").replace(")", "")
        : null,
      notes: server.ServerName.split(" ")[2]
        ? server.ServerName.split(" ")[2].replace("[", "").replace("]", "")
        : null,
      region: server.ServerRegion,
      active: server.IsActive,
      id: server.id,
      lastUpdate: new Date(),
    }));
    return normalizedData;
  } catch (error) {
    throw error;
  }
};
