const redis = require("../modules/redis");
const logger = require("../modules/logger");
const axios = require("../modules/axios");

module.exports = async (train) => {
  let timeTable = await redis.HGET("timetables:train", train.number);
  timeTable = JSON.parse(timeTable);
  if (timeTable && timeTable.expiry > Date.now()) {
    train.timeTable = filterPos(timeTable.timeTable);
    return train;
  } else {
    logger.info(`Fetching timeTable for train ${train.number}`);
    const response = await axios.get(
      `https://simrail.pwisnia.pl/api/getTrainRoute/${train.number}`
    );
    let timeTable = response.data;
    if (timeTable.status === "error") {
      logger.info(`Train ${train.number} not found`);
      return null;
    }
    timeTable = timeTable.data;
    redis.HSET(
      "timetables:train",
      train.number,
      JSON.stringify({
        timeTable,
        expiry: Date.now() + 1000 * 60 * 60 * 24,
      })
    );
    train.timeTable = filterPos(timeTable);
    return train;
  }
};

function filterPos(timetable) {
  return timetable.map((point) => point.nameOfPoint);
}
