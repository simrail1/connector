// https://panel.simrail.eu:8084/users-open/76561199567691192,76561199436906405?force=true

const axios = require("../modules/axios.js");
const redis = require("../modules/redis.js");
module.exports = async (steamIds) => {
  // remove null values
  steamIds = steamIds.filter((id) => id !== null);
  let toGet = [];
  for (let i = 0; i < steamIds.length; i++) {
    const playerData = await redis.HGET("players", steamIds[i]);
    if (
      !playerData ||
      Date.now() - JSON.parse(playerData).lastUpdate > 1000 * 60 * 60
    ) {
      toGet.push(steamIds[i]);
    }
  }
  try {
    if (toGet.length === 0) {
      return;
    }
    const { data } = await axios.get("/users-open/" + toGet.join(","));
    data.data.forEach((player) => {
      toGet = toGet.filter((id) => id !== player.SteamId);
      redis.HSET(
        "players",
        player.SteamId,
        JSON.stringify({
          ...player.SteamInfo[0],
          lastUpdate: Date.now(),
        })
      );
    });
  } catch (error) {
    throw error;
  } finally {
    if (toGet.length > 0) {
      toGet.forEach((id) => {
        redis.HSET(
          "players",
          id,
          JSON.stringify({
            lastUpdate: Date.now(),
          })
        );
      });
    }
  }
};
