require("dotenv").config();
const redis = require("./modules/redis.js");
const logger = require("./modules/logger.js");
const refreshData = require("./modules/refreshData.js");
async function main() {
  try {
    lastRefresh = await redis.HGET("refreshes", "connector");
    if (lastRefresh && Date.now() - lastRefresh < 10000) {
      return;
    }
    logger.info("Refreshing data");
    redis.HSET("refreshes", "connector", Date.now());
    await refreshData();
    logger.info("Refreshed data");
  } catch (error) {
    logger.error(error);
  }
}

const package = require("./package.json");

logger.info(`Starting ${package.name} v${package.version}`);

setInterval(main, 10000);
main();

